package main

import (
	"fmt"
)

const endPoint string = "kodingin/api"

func main() {
	fmt.Printf("%s", endPoint) // Output : "kodingin/api"
}
