package main

import (
	"fmt"
)

func todo() func() int {
	i := 3
	return func() int {
		i++
		return i
	}
}

func main() {
	dataTodo := todo()
	fmt.Println("Nilai toDo adalah", dataTodo())
}
