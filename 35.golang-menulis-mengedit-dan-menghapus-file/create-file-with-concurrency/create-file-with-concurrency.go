package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"sync"
)

func main() {

	data := make(chan int)
	var wg sync.WaitGroup

	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go func() {
			data <- rand.Int()
			wg.Done()
		}()

	}

	go func() {
		wg.Wait()
		close(data)
	}()

	// create file
	f, err := os.Create("note_for_today.txt")

	if err != nil {
		log.Fatal(err)
	}

	for v := range data {
		_, err := fmt.Fprintln(f, v) // memasukkan ke dalam baris(pengganti WriteString)
		fmt.Println(v)
		if err != nil {
			fmt.Println(err)
			f.Close()
		}
	}
}
