package main

import (
	"fmt"
	"log"
	"os"
)

const path = "note.txt"

func main() {
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		fmt.Println(err)
	}

	err = os.Remove(path)

	if err != nil {
		log.Fatal(err)
	}
}
