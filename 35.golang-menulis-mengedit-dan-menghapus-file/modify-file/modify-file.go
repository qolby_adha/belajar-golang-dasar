package main

import (
	"log"
	"os"
)

const path = "note.txt"

func main() {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}

	defer file.Close()

	file.WriteString("\n ini tambah baris\n")
	file.WriteString("4. Belajar Javascript\n")

	file.Sync()
}
