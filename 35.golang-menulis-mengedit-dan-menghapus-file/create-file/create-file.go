package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	c, err := os.Create("note.txt")

	if err != nil {
		log.Fatal(err)
	}

	c.WriteString("1. Belajar PHP\n")
	c.WriteString("2. Belajar Golang")

	defer c.Close()

	fmt.Println("selesai membuat file....")
}
