package main

import (
	"encoding/json"
	"fmt"
	"log"
)

func main() {

	mhsJSON := `{
					"0" : {
						"nama_depan"  : "Didk"
					},
					"1" : {
						"nama_depan"  : "Charly"
					}
				}`

	var dataMhs interface{}

	err := json.Unmarshal([]byte(mhsJSON), &dataMhs)

	if err != nil {
		log.Fatal(err)
	}

	for _, o := range dataMhs.(map[string]interface{}) {
		for _, u := range o.(map[string]interface{}) {
			fmt.Printf("Nama  : %v\n", u)
		}
	}
}