package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type Mahasiswa struct {
	NamaDepan    string `json:"nama_depan"`
	NamaBelakang string `json:"nama_belakang"`
}

func decodeJSON(data []byte) []Mahasiswa {

	var mhs []Mahasiswa

	err := json.Unmarshal(data, &mhs)

	if err != nil {
		log.Fatal(err)
	}
	return mhs
}

func main() {

	mhsJSON := `[{"nama_depan":"Didik","nama_belakang":"Prabowo"},
	{"nama_depan":"Charly","nama_belakang":"Van Houten"}]`

	dataMhs := decodeJSON([]byte(mhsJSON))

	for i, v := range dataMhs {
		fmt.Printf("%d : Nama Lengkap %v %v\n", i, v.NamaDepan, v.NamaBelakang)
	}
}
