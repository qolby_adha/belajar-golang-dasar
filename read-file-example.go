package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	file, err := os.Open("note.txt")

	if err != nil {
		log.Fatal(err)
	}
	toByte := make([]byte, 600)

	for {
		count, err := file.Read(toByte)

		if err != nil {
			break
		}

		fmt.Println(string(toByte[0:count]))
	}
}
