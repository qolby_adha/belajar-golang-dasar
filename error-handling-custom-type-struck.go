package main

import (
	"fmt"
)

type (
	customError struct {
		err string
		val int
	}
)

func (ce *customError) Error() string {
	return fmt.Sprintf("Nilainya %d: %s", ce.val, ce.err)
}
func checkAge(nilai int) (int, error) {
	if nilai <= 10 {
		return nilai, &customError{err: "Umut tidak di ijinkan", val: nilai}
	}

	return nilai, nil
}

func main() {
	checkAge, err := checkAge(8)
	if err != nil {
		if err, ok := err.(*customError); ok {
			fmt.Printf("Umur anda adalah %d\n", err.val)
			return
		}
		fmt.Println(err)
		return
	}
	fmt.Printf("Umur anda dalah %d \n", checkAge)
}
