package main

import (
	"fmt"
	"time"
)

func lamarPT1(ch chan string) {
	ch <- "Berhasil Melamar di PT 1"
}

func lamarPT2(ch chan string) {
	ch <- "Berhasil Melamar di PT 2"
}

func main() {
	lamar1 := make(chan string)
	lamar2 := make(chan string)

	go lamarPT1(lamar1)
	go lamarPT2(lamar2)
	time.Sleep(1 * time.Second)
	select {
	case l1 := <-lamar1:
		fmt.Println(l1)
	case l2 := <-lamar2:
		fmt.Println(l2)
	}
}
