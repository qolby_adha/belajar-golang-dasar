package main

import (
	"fmt"
)

func tambah(a int, b int) {
	c := a + b
	fmt.Println("Hasil Penjumlahan Antara", a, "+", b, "Adalah", c)
}

func main() {
	a := 10
	b := 20

	tambah(a, b)
}
