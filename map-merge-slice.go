package main

import (
	"fmt"
)

func main() {
	var mahasiswa = []map[string]string{
		map[string]string{"nama": "Didik Prabowo", "alamat": "Wonogiri"},
		map[string]string{"nama": "Rudi Subgyo", "alamat": "Solo"},
	}

	for i, v := range mahasiswa {
		fmt.Println(i, "= nama :", v["nama"], "alamat:", v["alamat"])
	}
}