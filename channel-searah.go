package main

import (
	"fmt"
)

func cetak(ch chan<- int) {
	ch <- 10
}

func main() {
	ch := make(chan int)

	go cetak(ch)

	fmt.Println(<-ch)
}
