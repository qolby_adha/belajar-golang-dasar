package main

import (
	"fmt"
)

func main() {

	fmt.Println("Array Multi Dimensi")
	const angka1 = 5
	const angka2 = 2
	var angka = [angka1][angka2]int{{0, 0}, {2, 2}, {3, 4}, {5, 6}, {7, 8}}

	for i := 0; i < angka1; i++ {
		for j := 0; j < angka2; j++ {
			fmt.Printf("Data ke [%d][%d] = %d\n", i, j, angka[i][j])
		}
	}
}
