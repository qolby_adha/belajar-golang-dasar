package main

import (
	"fmt"
)

func cetak(nama *string) {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("error", r)
		}
	}()

	if nama == nil {
		panic("Tidak boleh nil om")
	}
}

func main() {
	fmt.Println("Belajar Golang ke 33.\n")
	cetak(nil)
}
