package main

import (
	"errors"
	"fmt"
	"log"
)

func checkAge(nilai int) (bool, error) {
	if nilai <= 10 {
		return false, errors.New("Belum di ijinkan")
	}
	return true, nil

}

func main() {
	ageSatu := 10

	satu, err := checkAge(ageSatu)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(satu)
}
