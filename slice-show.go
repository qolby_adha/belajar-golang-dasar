package main

import (
	"fmt"
)

func main() {
	var mahasiswa []string
	mahasiswa = []string{"Didik Prabowo", "Charly Van Houten"}

	fmt.Println("Data Mahasiswa => ", mahasiswa)
	fmt.Println("Data Mahasiswa ke 0 sampai 1 => ", mahasiswa[0:1])
	fmt.Println("Data Mahasiswa ke 0 sampai 2 => ", mahasiswa[0:2])
	fmt.Println("Jumlah Mahasiswanya adalah => ", len((mahasiswa)))
}
