package main

import (
	"fmt"
)

func main() {

	v := func(nilai int) int {
		return nilai
	}

	result := checkNilaiA(v)
	name, nilai := checkNilaiB("didikprabowo", v)
	fmt.Println(name, nilai)
	fmt.Println(result)
}

func checkNilaiA(nilai func(int) int) int {
	return nilai(10)
}

func checkNilaiB(name string, nilai func(int) int) (string, int) {
	return name, nilai(100)
}
