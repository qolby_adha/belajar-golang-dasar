package main

import (
	"fmt"
)

func main() {

	go1 := func() {
		fmt.Println("Belajar Golang!")
	}
	go2 := func() {
		fmt.Println("Belajar PHP!")
	}

	go1()
	go2()
}
