// Simbol	Operator
// ==	Sama dengan
// !=	Tidak sama dengan
// <	Kurang dari
// <=	Kurang dari sama dengan
// >	Lebih dari
// >=	Lebih dari sama dengan
package main

import (
	"fmt"
)

func main() {

	//Set Nilai Awal
	angka1 := 10
	angka2 := 5

	angka3 := angka1 > angka2
	fmt.Println(angka3)
}
