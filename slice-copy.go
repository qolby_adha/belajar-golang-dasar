// Dimana b adalah data yang ingin kita copy, sedangkan a data yang menampung hasil copy.
// Untuk menampung hasil salinan dapat menggunakan fungsi make() di
// ikuti dengan jumlah element pada slice. Kita dapat menentukan jumlah berapa data yang ingin kita copy.

package main

import (
	"fmt"
)

func main() {
	biologi := []string{"Didik Prabowo", "Charly Van Houten"}
	fisika := make([]string, len(biologi))

	copy(fisika, biologi)
	fmt.Println(fisika)
}

// Pada kode di atas, variable fisika di tentukan dengan panjang
// element slice yang ada pada variable biologi dengan tujuan data pada variable biologi
// dapat di salin ke variable fisika secara keseluruhan.
