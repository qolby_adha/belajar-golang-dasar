package main

import (
	"fmt"
)

func recoverCetak() {
	if r := recover(); r != nil {
		fmt.Println("error", r)
	}
}

func cetak(nama *string) {
	defer recoverCetak()

	if nama == nil {
		panic("Tidak boleh nil om")
	}
}

func main() {
	fmt.Println("Belajar Golang ke 33.\n")
	cetak(nil)
}
