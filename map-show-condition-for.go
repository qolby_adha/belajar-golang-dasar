package main

import (
	"fmt"
)

func main() {
	var mahasiswa = map[string]string{"nama": "Didik Prabowo", "alamat": "Wonogiri"}
	// Delete Value Maps
	delete(mahasiswa, "alamat")

	for i, v := range mahasiswa {
		fmt.Println(i, "=", v)
	}

}
