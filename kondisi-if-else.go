package main

// kondisi ?: hasil

import (
	"fmt"
)

func main() {
	nilai := 10

	if nilai > 7 {
		fmt.Println("Anda Lulus dengan Nilai", nilai)
	} else {
		fmt.Println("Anda Tidak Lulus dengan Nilai", nilai)
	}

	var s = "Indonesias"
	x := false
	if x {
		fmt.Println("Anda berada di", s)
	} else {
		fmt.Println("Anda tidak berada di", s)
	}
}
