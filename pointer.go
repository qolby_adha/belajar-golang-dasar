Pointer merupakan sebuah variable yang berfungsi untuk menyimpan alamat memori variable lainnya.
Lokasi untuk menyimpan alamat memori yaitu pada RAM yang di pakai.


package main

import (
	"fmt"
)

func main() {

	var a int = 10
	var b *int = &a

	fmt.Println("Alamat Memori variable b adalah", b)
	fmt.Println("Nilai dari variable b adalah", *b)
}
// Perhatikan kode di atas, variable a memiliki tipe data integer dan mempunyai nilai yaitu 10.
// Sedangkan varible b memiliki tipe data integer dengan tipe pointer dan mengacu pada nilai variable a.
// Sehingga Alamat memori variable b berasal dari variable a dimana memiliki nilai 10.