// Map lebih bersifat fleksibel dari pada slice,
// dimana kita dapat menentukan tipe data key nya dan value nya.
// Sebagai contoh ada sebuah key dengan tipe data string dan value dengan tipe data integer.
// Namun dalam satu susunan map harus konsisten pada setiap elemen.

package main

import (
	"fmt"
)

func main() {
	var mahasiswa = map[string]string{}

	mahasiswa["Nama"] = "Didik Prabowo"
	mahasiswa["alamat"] = "Wonogiri"

	fmt.Println(mahasiswa)
}